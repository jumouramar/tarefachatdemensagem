function editMessage(pai){
    let content = pai.parentNode.firstElementChild.firstElementChild;
    var input = prompt("Qual será o novo conteúdo?");
    content.innerText = input;
}

function deleteMessage(pai){
    pai.parentNode.remove();
}

function createMessage(content){
   
   let messageC = document.createElement('div');
   let messageR = document.createElement('div');
   let contentP = document.createElement('p');
   let messageBtns = document.createElement('div');
   let btnEdit = document.createElement('button');
   let btnDelete = document.createElement('button');

    messageC.classList.add("message");
    messageR.classList.add("message_rectangle");
    contentP.classList.add("content");
    messageBtns.classList.add("btns");
    btnEdit.classList.add("btn_edit");
    btnDelete.classList.add("btn_delete");

    contentP.innerText = content;
    btnEdit.innerText = "Editar";

    btnEdit.addEventListener("click", () =>{
        editMessage(btnEdit.parentNode);
    })

    btnDelete.innerText = "Excluir";

    btnDelete.addEventListener("click", () =>{
        deleteMessage(btnDelete.parentNode);
    })

    messageC.append(messageR);
    messageC.append(messageBtns);
    messageBtns.append(btnEdit);
    messageBtns.append(btnDelete);
    messageR.append(contentP);

    let all_messages = document.querySelector(".all_messages");
    all_messages.append(messageC);
}


function postMessage(){
    let content = document.querySelector(".input_content").value;
    createMessage(content);
}

// LER A MENSAGEM PADRÃO
let buttonSend = document.querySelector(".btn_send")
buttonSend.addEventListener("click", () =>{
    postMessage();
})

let buttonDelete = document.querySelector(".btn_delete")
buttonDelete.addEventListener("click", () =>{
    deleteMessage(buttonDelete.parentNode);
})

let buttonEdit= document.querySelector(".btn_edit")
buttonEdit.addEventListener("click", () =>{
    editMessage(buttonEdit.parentNode);
})